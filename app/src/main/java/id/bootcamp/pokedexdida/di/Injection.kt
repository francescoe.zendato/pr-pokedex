package id.bootcamp.pokedexdida.di

import android.content.Context
import id.bootcamp.pokedexdida.data.ExampleRepository
import id.bootcamp.pokedexdida.data.PokedexRepository
import id.bootcamp.pokedexdida.data.local.ExampleSharedPreference
import id.bootcamp.pokedexdida.data.local.room.ExampleDatabase
import id.bootcamp.pokedexdida.data.local.room.PokedexDatabase
import id.bootcamp.pokedexdida.data.remote.retrofit.ApiConfig
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

object Injection {
    fun providePokedexRepository(context: Context):PokedexRepository{
        val pokedexDatabase = PokedexDatabase.getInstance(context)
        val pokedexDao = pokedexDatabase.pokemonDao()
        GlobalScope.launch { pokedexDao.getAllPokemon() }
        return PokedexRepository.getInstance(pokedexDao)
    }
    fun provideExampleRepository(context: Context):ExampleRepository{
        val exampleApiService = ApiConfig.getRegresApiService()
        val exampleDatabase = ExampleDatabase.getInstance(context)
        val exampleDao = exampleDatabase.exampleDao()
        val exampleSharedPreference = ExampleSharedPreference(context)
        return ExampleRepository.getInstance(exampleApiService,exampleDao,exampleSharedPreference)
    }
}