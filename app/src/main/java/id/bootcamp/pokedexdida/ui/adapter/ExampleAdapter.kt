package id.bootcamp.pokedexdida.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import id.bootcamp.pokedexdida.R
import id.bootcamp.pokedexdida.ui.data.ExampleData

class ExampleAdapter(val exampleDataList: ArrayList<ExampleData?>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val VIEW_TYPE_ITEM = 0
        private const val VIEW_TYPE_LOADING = 1
    }

    private var onExampleItemListener: OnExampleItemListener? = null

    fun setOnExampleItemListener(onExampleItemListener: OnExampleItemListener) {
        this.onExampleItemListener = onExampleItemListener
    }

    interface OnExampleItemListener {
        fun onItemClick(exampleData: ExampleData)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val ivAvatar = itemView.findViewById<CircleImageView>(R.id.ivAvatar)
        val tvName = itemView.findViewById<TextView>(R.id.tvName)
    }

    class LoadingHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val pgLoading = itemView.findViewById<ProgressBar>(R.id.progressBar)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == VIEW_TYPE_ITEM) {
            val view =
                LayoutInflater.from(parent.context).inflate(R.layout.item_example, parent, false)
            return ViewHolder(view)
        } else {
            val view =
                LayoutInflater.from(parent.context).inflate(R.layout.item_loading, parent, false)
            return LoadingHolder(view)
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolder) {
            val data = exampleDataList[position]
            if (data != null) {
                Glide.with(holder.itemView.context).load(data?.avatar).into(holder.ivAvatar)
                holder.tvName.text = "${data.firstName} ${data.lastName}"

                if (onExampleItemListener != null) {
                    holder.itemView.setOnClickListener {
                        onExampleItemListener?.onItemClick(data)
                    }
                }
            }
        }
    }


    override fun getItemCount(): Int {
        return exampleDataList?.size ?: 0
    }

    override fun getItemViewType(position: Int): Int {
        return if (exampleDataList?.get(position) == null) VIEW_TYPE_LOADING else VIEW_TYPE_ITEM
    }

    fun showLoading() {
        exampleDataList.add(null)
        notifyItemInserted(exampleDataList.lastIndex)
    }

    fun hideLoading() {
        exampleDataList.removeAt(exampleDataList.lastIndex)
        notifyItemRemoved(exampleDataList.lastIndex)
    }

    fun addData(dataList: List<ExampleData>) {
        exampleDataList.addAll(dataList)
        notifyDataSetChanged()
    }

    fun resetList() {
        exampleDataList.clear()
        notifyDataSetChanged()
    }
}