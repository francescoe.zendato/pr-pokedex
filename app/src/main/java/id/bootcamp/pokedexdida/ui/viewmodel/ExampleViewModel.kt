package id.bootcamp.pokedexdida.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.bootcamp.pokedexdida.data.ExampleRepository
import id.bootcamp.pokedexdida.ui.data.ExampleData
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class ExampleViewModel(private val exampleRepository: ExampleRepository) : ViewModel() {
    val exampleDataList: MutableLiveData<List<ExampleData>> = MutableLiveData()
    var page = 0
    var totalPage = 10
    fun getExampleDataList() = viewModelScope.launch {
//        val data = exampleRepository.getExampleListFromApi(page)
//        page = data.page
//        totalPage = data.totalPage
//
//        exampleDataList.postValue(data.data as List<ExampleData>)
        delay(2000)
        exampleDataList.postValue(getDataDummies())
    }

    fun getDataDummies() : List<ExampleData>{
        val dataList = ArrayList<ExampleData>()
        for (i in 0..10){
            val data = ExampleData("John $i","Doe $i","https://reqres.in/img/faces/6-image.jpg")
            dataList.add(data)
        }
        return dataList
    }
}