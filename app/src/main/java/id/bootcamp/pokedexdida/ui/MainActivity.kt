package id.bootcamp.pokedexdida.ui

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnScrollListener
import id.bootcamp.pokedexdida.R
import id.bootcamp.pokedexdida.databinding.ActivityMainBinding
import id.bootcamp.pokedexdida.ui.adapter.ExampleAdapter
import id.bootcamp.pokedexdida.ui.data.ExampleData
import id.bootcamp.pokedexdida.ui.viewmodel.ExampleViewModel
import id.bootcamp.pokedexdida.ui.viewmodel.ExampleViewModelFactory

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: ExampleViewModel
    private lateinit var mAdapter: ExampleAdapter
    private var isLoading = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //Inisiasi Viewmodel
        viewModel = ViewModelProvider(
            this,
            ExampleViewModelFactory.getInstance(this)
        ).get(ExampleViewModel::class.java)

        //Setup Action Bar
        setSupportActionBar(binding.searchBar)
//        supportActionBar?.title = "Example Data"

        //Setup Recyclerview
        mAdapter = ExampleAdapter(ArrayList())
        binding.rvExample.layoutManager = LinearLayoutManager(this)
        binding.rvExample.adapter = mAdapter

        //Aksi ketika scroll kebawah
        binding.rvExample.addOnScrollListener(object : OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val llManager = recyclerView.layoutManager as LinearLayoutManager
                if (!isLoading) {
                    if (llManager.findLastCompletelyVisibleItemPosition() == mAdapter.exampleDataList.lastIndex) {
                        if (viewModel.totalPage > viewModel.page) {
                            updateData()
                        }
                    }
                }
            }
        })

        //Observer menunggu data dari server
        viewModel.exampleDataList.observe(this) { dataList ->
            if (dataList != null) {
                mAdapter.hideLoading()
                //Update List
                mAdapter.addData(dataList)
                viewModel.exampleDataList.postValue(null)
                isLoading = false
            }
        }

        mAdapter.setOnExampleItemListener(object : ExampleAdapter.OnExampleItemListener {
            override fun onItemClick(exampleData: ExampleData) {
                val intent = Intent(this@MainActivity, MainDetailActivity::class.java)
                intent.putExtra(MainDetailActivity.EXTRA_EXAMPLE_DATA, exampleData)
                startActivity(intent)
            }
        })

        binding.swipeRefreshLayout.setOnRefreshListener {
            viewModel.page = 0
            mAdapter.resetList()
            updateData()
            binding.swipeRefreshLayout.isRefreshing = false
        }

        binding.searchView
            .editText
            .setOnEditorActionListener { textView, i, keyEvent ->
                binding.searchBar.setText(binding.searchView.text)
                binding.searchView.hide()
                false
            }

        if (mAdapter.exampleDataList.isEmpty()) {
            updateData()
        }
    }

    fun updateData() {
        isLoading = true
        mAdapter.showLoading()

        viewModel.page++
        viewModel.getExampleDataList()
    }

    //Set Menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    //Aksi di Menu
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menuData -> {
                val intent = Intent(this, CrudRoomActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}