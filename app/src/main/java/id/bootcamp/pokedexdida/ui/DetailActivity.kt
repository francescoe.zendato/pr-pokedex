package id.bootcamp.pokedexdida.ui

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import id.bootcamp.pokedexdida.data.local.entity.PokedexEntity
import id.bootcamp.pokedexdida.databinding.ActivityDetailBinding
import id.bootcamp.pokedexdida.ui.data.DetailData
import id.bootcamp.pokedexdida.ui.viewmodel.PokedexViewModel
import id.bootcamp.pokedexdida.ui.viewmodel.PokedexViewModelFactory

class DetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailBinding
    private lateinit var viewModel: PokedexViewModel
    private var id = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(this,PokedexViewModelFactory
            .getInstance(this))[PokedexViewModel::class.java]

        id = intent.getIntExtra("id",0)
        viewModel.getPokemonDetailById(id)
        viewModel.pokemonDetailData.observe(this) {
            if (it != null) {
                val link = toLink(it.name)
                val idS = toIdString(it.id)

                Glide.with(this).load(link).into(binding.ivArt)

                binding.tvName.text = it.name
                binding.tvType1.text = it.type1
                if (it.type2 != null) {
                    binding.tvType2.visibility = View.VISIBLE
                    binding.tvType2.text = it.type2
                }
                binding.tvID.text = idS
                binding.tvSpecies.text = it.species + " Pokémon"
                binding.tvEntry.text = it.entry
            }
        }
    }

    private fun toLink(name: String): String {
        var string = name.lowercase()
        if (string == "nidoran") string = "$string-f"
        return "https://img.pokemondb.net/artwork/large/$string.jpg"
    }

    private fun toIdString(id: Int): String {
        return "No. " + String.format("%03d", id)
    }
}