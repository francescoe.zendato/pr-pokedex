package id.bootcamp.pokedexdida.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import id.bootcamp.pokedexdida.R
import id.bootcamp.pokedexdida.ui.data.DetailData

class PokedexAdapter(val pokemonList: ArrayList<DetailData>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val VIEW_TYPE_ITEM = 0
        private const val VIEW_TYPE_LOADING = 1
    }

    private var onPokemonItemListener: OnPokemonItemListener? = null

    fun setOnItemListener(onPokemonItemListener: OnPokemonItemListener) {
        this.onPokemonItemListener = onPokemonItemListener
    }

    interface OnPokemonItemListener {
        fun onItemClick(data: DetailData)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val ivArt = itemView.findViewById<ImageView>(R.id.ivArt)
        val tvName = itemView.findViewById<TextView>(R.id.tvName)
        val tvID = itemView.findViewById<TextView>(R.id.tvID)
        val tvType1 = itemView.findViewById<TextView>(R.id.tvType1)
        val tvType2 = itemView.findViewById<TextView>(R.id.tvType2)
        val bDetail = itemView.findViewById<ImageButton>(R.id.bDetail)
    }

    class LoadingHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val pgLoading = itemView.findViewById<ProgressBar>(R.id.progressBar)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == VIEW_TYPE_ITEM) {
            val view =
                LayoutInflater.from(parent.context).inflate(R.layout.item_pokedex, parent, false)
            return ViewHolder(view)
        } else {
            val view =
                LayoutInflater.from(parent.context).inflate(R.layout.item_loading, parent, false)
            return LoadingHolder(view)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolder) {
            val data = pokemonList[position]
            val link = toLink(data.name)
            val id = toIdString(data.id)
            Glide.with(holder.itemView.context).load(link).into(holder.ivArt)
            holder.tvName.text = data.name
            holder.tvID.text = id
            holder.tvType1.text = data.type1
            if (data.type2 != null) {
                holder.tvType2.visibility = View.VISIBLE
                holder.tvType2.text = data.type2
            } else {
                holder.tvType2.visibility = View.GONE
            }
            if (onPokemonItemListener != null) {
                holder.bDetail.setOnClickListener {
                    onPokemonItemListener?.onItemClick(data)
                }
            }
        }
    }

    private fun toLink(name: String): String {
        var string = name.lowercase()
        if (string == "nidoran") string = "$string-f"
        return "https://img.pokemondb.net/artwork/large/$string.jpg"
    }

    private fun toIdString(id: Int): String {
        return "No. " + String.format("%03d", id)
    }

    override fun getItemCount(): Int {
        return pokemonList?.size ?: 0
    }

    override fun getItemViewType(position: Int): Int {
        return if (pokemonList?.get(position) == null) VIEW_TYPE_LOADING else VIEW_TYPE_ITEM
    }
}