package id.bootcamp.pokedexdida.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import id.bootcamp.pokedexdida.R
import id.bootcamp.pokedexdida.data.local.entity.PokedexEntity
import id.bootcamp.pokedexdida.databinding.ActivityPreviewBinding
import id.bootcamp.pokedexdida.ui.adapter.PokedexAdapter
import id.bootcamp.pokedexdida.ui.adapter.PokedexGridAdapter
import id.bootcamp.pokedexdida.ui.data.DetailData
import id.bootcamp.pokedexdida.ui.viewmodel.PokedexViewModel
import id.bootcamp.pokedexdida.ui.viewmodel.PokedexViewModelFactory

class PreviewActivity : AppCompatActivity() {
    private lateinit var binding: ActivityPreviewBinding
    private lateinit var viewModel: PokedexViewModel
    private lateinit var pAdapter: PokedexAdapter
    private lateinit var pAdapterGrid: PokedexGridAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPreviewBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(this,PokedexViewModelFactory
            .getInstance(this))[PokedexViewModel::class.java]

        supportActionBar?.title = "Pokédex"

        viewModel.pokemonListData.observe(this) {
            if (it != null) {
                binding.rvPokePreview.layoutManager = LinearLayoutManager(this)
                pAdapter = PokedexAdapter(ArrayList(it))
                pAdapterGrid = PokedexGridAdapter(ArrayList(it))
                binding.rvPokePreview.adapter = pAdapter

                pAdapter.setOnItemListener(object : PokedexAdapter.OnPokemonItemListener{
                    override fun onItemClick(data: DetailData) {
                        val intent = Intent(this@PreviewActivity,
                            DetailActivity::class.java)
                        intent.putExtra("id",data.id)
                        startActivity(intent)
                    }
                })

                pAdapterGrid.setOnItemListener(object : PokedexGridAdapter.OnPokemonItemListener{
                    override fun onItemClick(data: DetailData) {
                        val intent = Intent(this@PreviewActivity,
                            DetailActivity::class.java)
                        intent.putExtra("id",data.id)
                        startActivity(intent)
                    }
                })
            }
        }
        viewModel.getAllPokemon()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.pokedex_menu,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.mProfile -> {
                intent = Intent(this@PreviewActivity, ProfileActivity::class.java)
                startActivity(intent)
            }
            R.id.mGrid -> {
                binding.rvPokePreview.layoutManager = GridLayoutManager(this,2)
                binding.rvPokePreview.adapter = pAdapterGrid
            }
            R.id.mList -> {
                binding.rvPokePreview.layoutManager = LinearLayoutManager(this)
                binding.rvPokePreview.adapter = pAdapter
            }
        }
        return super.onOptionsItemSelected(item)
    }
}