package id.bootcamp.pokedexdida.ui.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import id.bootcamp.pokedexdida.data.ExampleRepository
import id.bootcamp.pokedexdida.data.PokedexRepository
import id.bootcamp.pokedexdida.di.Injection

class PokedexViewModelFactory private constructor(private val pokedexRepository: PokedexRepository) :
    ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PokedexViewModel::class.java)) {
            return PokedexViewModel(pokedexRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class: " + modelClass.name)
    }

    companion object {
        @Volatile
        private var instance: PokedexViewModelFactory? = null
        fun getInstance(context: Context): PokedexViewModelFactory =
            instance ?: synchronized(this) {
                instance ?: PokedexViewModelFactory(Injection.providePokedexRepository(context))
            }.also { instance = it }
    }
}