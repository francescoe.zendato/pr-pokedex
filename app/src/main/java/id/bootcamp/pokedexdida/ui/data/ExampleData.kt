package id.bootcamp.pokedexdida.ui.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ExampleData(
    val firstName: String,
    val lastName: String,
    val avatar: String
) : Parcelable
