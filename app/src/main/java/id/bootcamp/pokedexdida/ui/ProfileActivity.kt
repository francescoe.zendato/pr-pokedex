package id.bootcamp.pokedexdida.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import id.bootcamp.pokedexdida.R
import id.bootcamp.pokedexdida.databinding.ActivityProfileBinding
import id.bootcamp.pokedexdida.ui.viewmodel.PokedexViewModel
import id.bootcamp.pokedexdida.ui.viewmodel.PokedexViewModelFactory

class ProfileActivity : AppCompatActivity() {
    private lateinit var binding: ActivityProfileBinding
    private lateinit var viewModel: PokedexViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(this,PokedexViewModelFactory
            .getInstance(this))[PokedexViewModel::class.java]
    }
}