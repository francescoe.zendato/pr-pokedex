package id.bootcamp.pokedexdida.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.bootcamp.pokedexdida.data.PokedexRepository
import id.bootcamp.pokedexdida.data.local.entity.PokedexEntity
import id.bootcamp.pokedexdida.ui.data.DetailData
import kotlinx.coroutines.launch

class PokedexViewModel(val pokedexRepository: PokedexRepository) : ViewModel() {
    val pokemonListData = MutableLiveData<List<DetailData>>()
//    var pokemonDetailData = DetailData()
    var pokemonDetailData = MutableLiveData<DetailData>()
    fun getAllPokemon() = viewModelScope.launch {
        val data = pokedexRepository.getAllPokemon()
        pokemonListData.postValue(data)
    }
    fun getPokemonDetailById(id: Int) = viewModelScope.launch {
        val data = pokedexRepository.getPokemonDetailById(id)
        pokemonDetailData.postValue(data)
    }
}