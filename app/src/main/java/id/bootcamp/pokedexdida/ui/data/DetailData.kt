package id.bootcamp.pokedexdida.ui.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class DetailData(
    val name: String = "",
    val type1: String = "",
    val type2: String? = null,
//    val type1: Int = 0,
//    val type2: Int? = null,
    val id: Int = 0,
    val species: String = "",
    val entry: String? = null
) : Parcelable
