package id.bootcamp.pokedexdida.data.local.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "pokedex")
class PokedexEntity (

    @field:ColumnInfo(name = "id")
    @field:PrimaryKey
    val id: Int = 0,

    @field:ColumnInfo(name = "national_id")
    val nationalId: Int? = null,

    @field:ColumnInfo(name = "name")
    val name : String? = null,

    @field:ColumnInfo(name = "sprite_id")
    val spriteId : Int? = null,

    @field:ColumnInfo(name = "type_id_1")
    val typeId1 : Int? = null,

    @field:ColumnInfo(name = "type_id_2")
    val typeId2 : Int? = null,

    @field:ColumnInfo(name = "species")
    val species : String? = null

) : Parcelable