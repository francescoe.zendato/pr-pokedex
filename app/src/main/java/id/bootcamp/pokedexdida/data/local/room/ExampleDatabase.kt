package id.bootcamp.pokedexdida.data.local.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import id.bootcamp.pokedexdida.data.local.entity.ExampleEntity

@TypeConverters(DateConverter::class)
@Database(entities = [ExampleEntity::class], version = 3, exportSchema = false)
abstract class ExampleDatabase : RoomDatabase() {

    abstract fun exampleDao(): ExampleDao

    companion object {
        @Volatile
        private var instance: ExampleDatabase? = null
        fun getInstance(context: Context): ExampleDatabase =
            instance ?: synchronized(this) {
                instance ?: Room.databaseBuilder(
                    context.applicationContext,
                    ExampleDatabase::class.java, "example.db"
                )
                    //.createFromAsset("database/example.db")
                    .fallbackToDestructiveMigration()
                    .build()
            }
    }
}