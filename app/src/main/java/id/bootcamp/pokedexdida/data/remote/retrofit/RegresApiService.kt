package id.bootcamp.pokedexdida.data.remote.retrofit

import id.bootcamp.pokedexdida.data.remote.request.ReqresCreateRequest
import id.bootcamp.pokedexdida.data.remote.response.ReqresCreateResponse
import id.bootcamp.pokedexdida.data.remote.response.ReqresListUserResponse
import id.bootcamp.pokedexdida.data.remote.response.ReqresSingleUserResponse
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface RegresApiService {

    @GET("users")
    suspend fun getUser(@Query("page") page :Int): ReqresListUserResponse

    @GET("users/{id}")
    suspend fun getUserById(@Path("id") id : Int): ReqresSingleUserResponse

    @POST("users")
    suspend fun insertUser(@Body reqresCreateRequest: ReqresCreateRequest) : ReqresCreateResponse
}