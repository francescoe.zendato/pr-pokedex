package id.bootcamp.pokedexdida.data.local.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "entry")
class EntryEntity (

    @field:ColumnInfo(name = "id")
    @field:PrimaryKey
    val id: Int = 0,

    @field:ColumnInfo(name = "pokedex_id")
    val pokedexId: Int? = null,

    @field:ColumnInfo(name = "entry_1")
    val entry1 : String? = null,

    @field:ColumnInfo(name = "entry_2")
    val entry2 : String? = null,

    @field:ColumnInfo(name = "entry_3")
    val entry3 : String? = null,

    @field:ColumnInfo(name = "entry_4")
    val entry4 : String? = null,

    @field:ColumnInfo(name = "entry_5")
    val entry5 : String? = null,

) : Parcelable