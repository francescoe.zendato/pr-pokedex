package id.bootcamp.pokedexdida.data.local.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import id.bootcamp.pokedexdida.data.local.entity.EntryEntity
import id.bootcamp.pokedexdida.data.local.entity.PokedexEntity
import id.bootcamp.pokedexdida.data.local.entity.TypeEntity

@TypeConverters(DateConverter::class)
@Database(entities = [PokedexEntity::class, EntryEntity::class, TypeEntity::class], version = 6,
    exportSchema = true)
abstract class PokedexDatabase : RoomDatabase() {

    abstract fun pokemonDao(): PokedexDao

    companion object {
        @Volatile
        private var instance: PokedexDatabase? = null
        fun getInstance(context: Context): PokedexDatabase =
            instance ?: synchronized(this) {
                instance ?: Room.databaseBuilder(
                    context.applicationContext,
                    PokedexDatabase::class.java,"pokemon.db"
                )
//                    .addMigrations(migV3toV4)
                    .createFromAsset("database/pokemon.db")
                    .fallbackToDestructiveMigration()
                    .build()
            }

        val migV3toV4 = object : Migration(3,4) {
            override fun migrate(db: SupportSQLiteDatabase) {
            }
        }
    }
}