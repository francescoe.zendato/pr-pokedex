package id.bootcamp.pokedexdida.data.local.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "type")
class TypeEntity (

    @field:ColumnInfo(name = "id")
    @field:PrimaryKey
    val id: Int = 0,

    @field:ColumnInfo(name = "name")
    val name : String? = null,

) : Parcelable