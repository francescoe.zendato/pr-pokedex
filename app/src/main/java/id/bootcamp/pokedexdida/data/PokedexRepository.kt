package id.bootcamp.pokedexdida.data

import id.bootcamp.pokedexdida.data.local.entity.PokedexEntity
import id.bootcamp.pokedexdida.data.local.room.PokedexDao
import id.bootcamp.pokedexdida.ui.data.DetailData

class PokedexRepository private constructor(
    private val pokedexDao: PokedexDao
) {
    suspend fun getAllPokemon() : List<DetailData> {
        return pokedexDao.getAllPokemon()
    }

    suspend fun getPokemonDetailById(id: Int): DetailData {
        return pokedexDao.getPokemonDetailById(id)
    }
    companion object {
        @Volatile
        private var instance: PokedexRepository? = null
        fun getInstance(pokedexDao: PokedexDao): PokedexRepository =
            instance ?: synchronized(this) {
                instance ?: PokedexRepository(pokedexDao)
            }.also { instance = it }
    }
}