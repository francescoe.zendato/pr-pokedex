package id.bootcamp.pokedexdida.data.local

import android.content.Context

class ExampleSharedPreference(context: Context) {
    companion object {
        private const val PREFS_NAME = "pref"
        private const val EMAIL_KEY = "email"
        private const val TOKEN_KEY = "token"
    }

    private val preference = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    fun saveLoginData(email: String, token: String) {
        val editor = preference.edit()
        editor.putString(EMAIL_KEY, email)
        editor.putString(TOKEN_KEY, token)
        editor.apply()
    }

    fun getEmailData(): String {
        return preference.getString(EMAIL_KEY, "") ?: ""
    }

    fun getTokenData(): String {
        return preference.getString(TOKEN_KEY, "") ?: ""
    }

    fun logOut() {
        val editor = preference.edit()
        editor.putString(EMAIL_KEY, null)
        editor.putString(TOKEN_KEY, null)
        editor.apply()
    }

    fun isLogin(): Boolean {
        val token = getTokenData()
        if (token.isNotEmpty()) {
            return true
        } else {
            return false
        }
    }


}