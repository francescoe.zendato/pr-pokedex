package id.bootcamp.pokedexdida.data.local.room

import androidx.room.Dao
import androidx.room.Query
import id.bootcamp.pokedexdida.data.local.entity.PokedexEntity
import id.bootcamp.pokedexdida.ui.data.DetailData

@Dao
interface PokedexDao {
    @Query("SELECT p.name, t1.name as type1, t2.name as type2, p.id, p.species," +
            "coalesce(e.entry_1, e.entry_2, e.entry_3, e.entry_4) as entry FROM POKEDEX as p " +
            "left join type as t1 on p.type_id_1 = t1.id " +
            "left join type as t2 on p.type_id_2 = t2.id " +
            "left join entry as e on p.id = e.pokedex_id")
    suspend fun getAllPokemon(): List<DetailData>

    @Query("SELECT p.name, t1.name as type1, t2.name as type2, p.id, p.species," +
            "coalesce(e.entry_1, e.entry_2, e.entry_3, e.entry_4) as entry FROM POKEDEX as p " +
            "left join type as t1 on p.type_id_1 = t1.id " +
            "left join type as t2 on p.type_id_2 = t2.id " +
            "left join entry as e on p.id = e.pokedex_id where p.id = :id")
    suspend fun getPokemonDetailById(id: Int): DetailData

}