package id.bootcamp.pokedexdida.data.remote.request

import kotlinx.parcelize.Parcelize
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

@Parcelize
data class ReqresCreateRequest(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("job")
	val job: String? = null
) : Parcelable
